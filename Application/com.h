/*
 * com.h
 *
 *  Created on: 8 Nov 2017
 *      Author: Standa
 */

#ifndef APPLICATION_COM_H_
#define APPLICATION_COM_H_

#include "error.h"
#include <stdint.h>


extern err_Td com_Init(void);
extern err_Td com_SendF(char *format, ...);
extern err_Td com_Receive(uint8_t* buf, uint32_t len);
extern void com_LoopTask(void);

extern err_Td com_SetCANSpeed(uint16_t baudrate_kbps);
extern uint16_t com_GetBaudrate(void);
extern err_Td com_SetTxInterruption(uint16_t start_us, uint16_t duration_us);
extern void com_CanTxPinChanged(void);
extern void com_DelayElapsed(void);
extern void com_DurationPassed(void);

extern void com_SetByteOrder(uint8_t mode);
extern uint8_t com_GetByteOrder(void);


#endif /* APPLICATION_COM_H_ */
