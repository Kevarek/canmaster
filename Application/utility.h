/*
 * utility.h
 *
 *  Created on: 8 Nov 2017
 *      Author: Standa
 */

#ifndef UTILITY_H_
#define UTILITY_H_


#define UT_ISNAN(x)			        ( (x) != (x) )
#define UT_SIZEOFARRAY(x)		    ( sizeof( (x) ) / sizeof( (x[0]) ) )
#define UT_VALINRANGE(val,min,max)  ( ( val >= (min) ) && ( val <= (max) ) )
#define UT_SATURATE(val, min, max)	if( (val) > (max) ){ (val) = (max); }\
									else if( (val) < (min) ){ (val) = (min); }
#define UT_SAFEASSIGN(ptr,val)		if( (ptr) ){ ( *(ptr) = (val) ); };


extern float ut_DegToRad(float deg);
extern float ut_RadToDeg(float rad);


#endif /* UTILITY_H_ */
