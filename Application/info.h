/*
 * info.h
 *
 *  Created on: 22 Feb 2020
 *      Author: Standa
 */

#ifndef INFO_H_
#define INFO_H_

#define INFO_COMPANY	"NA"
#define INFO_AUTHOR		"StanislavSubrt"
#define INFO_DEVICE		"CANMaster"
#define INFO_HWVER		"RevA"
#define INFO_FWVER		"2.0.1"
#define INFO_ID			"1"
#define INFO_CALDATE	"18.4.2020"


//FW 1.0.1: CAN communication implemented
//FW 2.0.1: Interruption of CAN TX messages implemented

#endif /* INFO_H_ */
